package com.dslab.ecommercelab.controller;


import com.dslab.ecommercelab.entity.Order;

import com.dslab.ecommercelab.entity.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/order")
public class OrderController  {

    @Autowired
    OrderRepository repository;

    @PostMapping(path = "/add")
    public @ResponseBody Order addOrder(@RequestBody Order order){
        return repository.save(order);
    }

    @GetMapping(path = "{/id}")
    public @ResponseBody Order getProduct(@PathVariable Integer id ){

        Optional<Order> byId = repository.findById(id);
        return  byId.get();

    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Order> getAll(){
        return  repository.findAll();
    }

    @DeleteMapping (path = "/{id}")
    private String  deleteOrder(@PathVariable Integer id){
        return String.format("The order with id %d has been deleted!",id);
    }


}
