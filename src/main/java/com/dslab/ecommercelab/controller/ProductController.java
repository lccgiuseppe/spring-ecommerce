package com.dslab.ecommercelab.controller;

import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.entity.ProductRepository;
import com.dslab.ecommercelab.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping (path = "/product")
public class ProductController {

    @Autowired
    ProductRepository repository;

    @PostMapping(path = "/add")
    public @ResponseBody Product addProduct( @RequestBody Product product){
        return repository.save(product);
    }

    @GetMapping(path = "{/id}")
    public @ResponseBody Product getProduct(@PathVariable Integer id ){

        Optional<Product> byId = repository.findById(id);
        return  byId.get();

    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Product> getAll(){
         return  repository.findAll();
    }

    @DeleteMapping (path = "/{id}")
    private String  deleteProduct(@PathVariable Integer id){
        return String.format("The product with id %d has been deleted!",id);
    }

}
