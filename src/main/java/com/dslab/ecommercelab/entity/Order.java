package com.dslab.ecommercelab.entity;


import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@javax.persistence.Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id", insertable = false, updatable = false)
    private User user;

    private enum State {
        nuovo, evaso
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private State state;

    @ElementCollection
    private List<Product> productList;

    public int getId() {
        return id;
    }


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public double getTotal(){
        double totalPrice = 0.0;

            for ( Product prod : productList)
            {
             totalPrice = totalPrice + prod.getPrice() ;
            }
        return totalPrice;
    }
}
