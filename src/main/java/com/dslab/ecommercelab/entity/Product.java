package com.dslab.ecommercelab.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@javax.persistence.Entity
public class Product {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int id;

    @NotNull(message = "Campo brand obbligatorio")
    private String brand;

    @NotNull(message = "Campo model obbligatorio")
    private String model;

    @Basic
    private String description;

    @NotNull(message = "Il campo price è obbligatorio")
    private double price;

    @NotNull(message = "Il campo items è obbligatorio")
    private int items;

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }
}
